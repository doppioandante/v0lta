#ifndef BATTERY_H_INCLUDED
#define BATTERY_H_INCLUDED

enum battery_status {
	BATTERY_UNKNOWN,
	BATTERY_NOT_FOUND,
	BATTERY_ERRNO,
	BATTERY_UNPLUGGED,
	BATTERY_OK
};

enum battery_status battery_open(void);
enum battery_status battery_get(unsigned char*);
enum battery_status battery_close(void);

#endif
