#ifndef DRAW_H_INCLUDED
#define DRAW_H_INCLUDED

enum charger_lvgl_backend {
    CHARGER_LVGL_BACKEND_SDL,
    CHARGER_LVGL_BACKEND_FB
};

lv_disp_t* initialize_screen(enum charger_lvgl_backend backend);
void deinit_screen(enum charger_lvgl_backend backend);

int calculate_font_size();

void style_battery_icon(lv_obj_t* bat_bar, lv_obj_t* bat_body_top);
void position_battery_icon(lv_obj_t* bat_bar, lv_obj_t* bat_body_top);

void style_battery_label(lv_obj_t* bat_label, lv_font_t* bat_label_font);
void position_battery_label(lv_obj_t* bat_label, lv_obj_t* bat_bar);

#endif
