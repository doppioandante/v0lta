#include <stdint.h>
#include <stdio.h>
#include <time.h>

uint32_t get_ms() {
	static struct timespec t;
	int err = clock_gettime(CLOCK_REALTIME, &t);
	if(err == -1) perror("clock_gettime");
	return t.tv_sec * 1000 + t.tv_nsec / 1000000;
}

