# charging-lvgl
WIP replacement for [charging-sdl](https://gitlab.com/postmarketOS/charging-sdl)

## TODO
* [x] Get rid of canvas
* [x] Implement progress bar
* [x] Dynamic screen resolution
* [x] Implement framebuffer backend
* [x] Implement getting battery information without SDL
* [x] Make fonts scale
* [ ] Make code look pretier
* [x] ~~Revisit Makefile (literally stolen from lv_examples)~~ glory to meson
* [ ] maybe also smth else i forgot lol

## Building
* Install devel package for freetype2
* `meson build`
* `meson compile -C build`

## Running
`./charging-lvgl <path to TTF font>`

## Screenshots
not today, ha
